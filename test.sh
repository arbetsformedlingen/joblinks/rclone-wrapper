#!/usr/bin/env bash
set -eEu -o pipefail

self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# get command line arguments
secrets="$1"
bucket="$2"
testdir="$bucket/$3" # remote, in bucket

if [ -z "$secrets" ] || [ -z "$bucket" ] || [ -z "$testdir" ]; then
    echo "**** supply secrets, bucket and directory for tests" >&2; exit 1
fi


# local test directory
#localdir=$(mktemp -d)
localdir=/tmp/xxyy
mkdir $localdir

# test runner
function run_test() {
    local name="$1"; shift
    local op="$1"; shift
    local src="$1"; shift
    local trg="$1"; shift
    local expected_status="$1"; shift

    set +e
    "$self_dir"/rclone.sh --secrets "$secrets" --op "$op" --src "$src" --trg "$trg" $@
    if [ $? != "$expected_status" ]; then
        echo -e "**** test $name:\tfail" >&2; exit 1
    else
        echo -e "**** test $name:\tpass" >&2
    fi
    set -e
    echo >&2
}



##### tests

### upload0: source does not exist
run_test upload0 "copy" "$localdir/upload0" "$bucket:$testdir/upload0" 1 --flock



### upload1: source exists, no remote exists: should fail as flock won't happen
echo "line A" > "$localdir"/db0
run_test upload1 "copy" "$localdir/db0" "$bucket:$testdir/db0" 1 --flock



### upload2: source exists, remote exists:
echo "line A" > "$localdir"/db1
"$self_dir"/rclone.sh --secrets "$secrets" --op copy --src "$localdir/db1" --trg "$bucket:$testdir/db1"
run_test download0 "copy" "$bucket:$testdir/db1" "$localdir/db1" 0 --flock
run_test upload2 "copy" "$localdir/db1" "$bucket:$testdir/db1" 0 --flock



### upload3: source exists, remote exists: lockfile changed on remote
echo "line A" > "$localdir"/db2
"$self_dir"/rclone.sh --secrets "$secrets" --op copy --src "$localdir/db2" --trg "$bucket:$testdir/db2"
rm -f "${localdir}"/*
mc ls pipeline-test/pipeline-test/rclone-validate | grep db2
run_test download0 "copy" "$bucket:$testdir/db2" "$localdir/db2" 0 --flock
# tamper with remote lock file:
echo changes > "$localdir"/newlock
"$self_dir"/rclone.sh --secrets "$secrets" --op copy --src "$localdir/newlock" --trg "$bucket:$testdir/db2.md5"
rm -f "$localdir"/newlock
# try to upload
run_test upload3 "copy" "$localdir/db2" "$bucket:$testdir/db2" 1 --flock


### upload4: upload and download * 2, check final file
echo "line A" > "$localdir"/db3
"$self_dir"/rclone.sh --secrets "$secrets" --op copy --src "$localdir/db3" --trg "$bucket:$testdir/db3"
rm -f "${localdir}"/db3
nrtests=20
for s in $(seq 1 "$nrtests"); do
    run_test download1."$s" "copy" "$bucket:$testdir/db3" "$localdir/db3" 0 --flock
    mc rm "$bucket/$testdir/db3"
    echo "$nrtests" > "${localdir}"/db3
    run_test upload4."$s" "copy" "$localdir/db3" "$bucket:$testdir/db3" 0 --flock
    rm -f "${localdir}"/db3
done
run_test download2 "copy" "$bucket:$testdir/db3" "$localdir/db3" 0 --flock
if [ "$(cat $localdir/db3)" = "$nrtests" ]; then
    echo "multiple ul/dl: pass" >&2
else
    echo "multiple ul/dl: fail" >&2
fi
