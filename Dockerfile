FROM docker.io/library/ubuntu:22.04 AS base

COPY *.sh /usr/local/bin/

RUN apt-get -y update &&\
        apt-get -y install -yq --no-install-recommends --fix-missing rclone ca-certificates wget &&\
        wget -O /usr/local/bin/mc https://dl.min.io/client/mc/release/linux-amd64/archive/mc.RELEASE.2024-02-09T22-18-24Z &&\
        chmod +x /usr/local/bin/mc &&\
        apt-get clean && apt-get -y autoremove &&\
        rm -rf /var/lib/apt/lists/*


###############################################################################
FROM base AS test

RUN  rclone --version \
     && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

#need writable home for the "mc" steps in the rclone wrapper
WORKDIR /tmp
ENV HOME /tmp
ENTRYPOINT [ "bash", "/usr/local/bin/rclone.sh" ]
