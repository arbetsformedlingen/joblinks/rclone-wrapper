#!/usr/bin/env bash
set -eEu -o pipefail

bucket=pipeline-test
remotedir=rclone-validate2

number_of_iterations=5
copies_per_iteration=100

workdir=$(mktemp -d)
cd "${workdir}" || exit 1


function create_remote_testfile() {
    local testfile=test_"$RANDOM""$RANDOM".txt
    local copyfails=0

    #echo "**** make sure remote test directory is clear" >&2
    mc rm --force --recursive "${bucket}"/"${bucket}"/"${remotedir}"/ >/dev/null

    #echo "**** create test file" >&2
    echo hej > "${testfile}"

    set +e
    while true; do
        #echo "**** copy test file to remote" >&2
        rclone --retries 100 copyto "${testfile}" "${bucket}":"${bucket}"/"${remotedir}"/"${testfile}"

        #echo "**** check that it arrived" >&2
        if mc ls "${bucket}"/"${bucket}"/"${remotedir}" | grep -q "${testfile}"; then break; fi

        #echo "**** upload of testfile failed, retrying" >&2

        copyfails=$(( copyfails + 1 ))
    done
    set -e

    echo "$copyfails,${testfile}"
}


function copy_testsuite0() {
    local src="$1"
    local trg="$2"

    rclone copyto "$src" "$trg"
}
export -f copy_testsuite0


function copy_testsuite1() {
    local src="$1"
    local trg="$2"

    rclone --retries 10 copyto "$src" "$trg"
}
export -f copy_testsuite1


function copy_testsuite2() {
    local src="$1"
    local trg="$2"

    rclone --retries 10 copyto "$src" "$trg"
    mc ls $(echo "$trg" | tr ':' '/') >/dev/null 2>&1
}
export -f copy_testsuite2


function run_tests() {
    local name="$1"
    local desc="$2"
    local copy_function="$3"

    local sum_copyfails=0

    echo -n "**** Run $name $number_of_iterations number of times. $desc. Success rates: " >&2
    for s in $(seq 1 "$number_of_iterations"); do
        count_success=0
        for i in $(seq 1 "$copies_per_iteration"); do
            local testdata=$(create_remote_testfile)
            local copyfails="$(echo $testdata | sed 's|,.*$||')"
            local testfile="$(echo $testdata | sed 's|^.*,||')"
            sum_copyfails=$(( sum_copyfails + copyfails ))

            #mc ls "${bucket}"/"${bucket}"/"${remotedir}"/"${testfile}" >/dev/null
            "$copy_function" "${bucket}":"${bucket}"/"${remotedir}"/"${testfile}" "${testfile}"

            if [ -f "${testfile}" ]; then
                count_success=$(( count_success + 1 ))
            fi

            rm -f "${testfile}"
        done
        echo -n "[$count_success/$copies_per_iteration] " >&2
    done
    echo >&2
    echo >&2

    echo "${sum_copyfails}"
}


copyfails0=$(run_tests testsuite0 "Plain 'rclone copyto'" copy_testsuite0)
echo "Number of upload fails when setting up a remote test file: ${copyfails0}" >&2

copyfails1=$(run_tests testsuite1 "Activate retry, 'rclone --retries 10 copyto'" copy_testsuite1)
echo "Number of upload fails when setting up a remote test file: ${copyfails1}" >&2

copyfails2=$(run_tests testsuite2 "Activate retry, and add a seemingly unrelated 'mc ls' after each copy" copy_testsuite2)
echo "Number of upload fails when setting up a remote test file: ${copyfails2}" >&2
